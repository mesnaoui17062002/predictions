from django.contrib import admin

# Register your models here.
from . import models

admin.site.register(models.Estate_Record)

admin.AdminSite.site_header = 'Immo Administration'
admin.AdminSite.site_title = 'Estate Record Management'
admin.AdminSite.index_title = 'Dashboard'

