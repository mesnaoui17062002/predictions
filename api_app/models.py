# Create your models here.

from django.db import models

class Estate_Record(models.Model):
    """This model includes conditions that influence the price of the estate"""
    CRIM = models.FloatField()
    ZN = models.FloatField()
    INDUS = models.FloatField()
    CHAS = models.FloatField()
    NOX = models.FloatField()
    RM = models.FloatField()
    AGE = models.FloatField()
    DIS = models.FloatField()
    RAD = models.FloatField()
    TAX = models.FloatField()
    PTRATIO = models.FloatField()
    LSTAT = models.FloatField()
    MEDV = models.FloatField()
    YEAR = models.IntegerField()
    ESTATEID = models.IntegerField()

    def __str__(self):
        return  "Estate ID: %s"% (self.ESTATEID)
    