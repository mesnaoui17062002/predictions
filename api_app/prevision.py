from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, PolynomialFeatures
from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error
from .models import Estate_Record
import pandas as pd
import numpy as np
from statsmodels.tsa.arima.model import ARIMA
from keras.models import Sequential
from keras.layers import LSTM, Dense

def get_data(estate_id):
    # Query the Estate object with the given estate_id
    estates = Estate_Record.objects.filter(ESTATEID=estate_id).order_by('YEAR')
    if len(estates) == 0:
        return None
    return  estates

def make_record(crim,zn,indus,chas,nox,rm,age,dis,rad,tax,ptratio,lstat,medv,year,id):
    nouvel_objet = Estate_Record.objects.create(
        CRIM= crim,
        ZN= zn,
        INDUS= indus,
        CHAS= chas,
        NOX= nox,
        RM= rm,
        AGE= age,
        DIS= dis,
        RAD= rad,
        TAX= tax,
        PTRATIO= ptratio,
        LSTAT= lstat,
        MEDV= medv,
        YEAR= year,
        ESTATEID= id
    )
    nouvel_objet.save()

def modif_record(crim,zn,indus,chas,nox,rm,age,dis,rad,tax,ptratio,lstat,medv,year,id):
    record = Estate_Record.objects.get(ESTATEID=id, YEAR=year)
    record.CRIM = crim
    record.ZN = zn
    record.INDUS = indus
    record.CHAS = chas
    record.NOX = nox
    record.RM = rm
    record.AGE = age
    record.DIS = dis
    record.RAD = rad
    record.TAX = tax
    record.PTRATIO = ptratio
    record.LSTAT = lstat
    record.MEDV = medv
    record.save()

def predict_future_prices(pre_estates):
    estates = [[pre_estates[j][i] for j in range(len(pre_estates))] for i in range(14)]
    prices  = estates[12]
    ys = estates[13]
    y=ys[-1]
    for i in range(6):
        ys.append(y + i + 1)
    print(ys)
    Model1 = arima_predictions(np.array(estates[12]))
    Model2 = lstm_predictions(np.array(estates[12]))
    prices_pred = [ Model1[0] , Model2[0]][best_predictions(Model1[1]-Model2[1],Model1[2]-Model2[2],Model1[3]-Model2[3])]

    return (list(prices) + list(prices_pred), ys)



def arima_predictions(conditions):
    # Entraîner le modèle ARIMA
    model = ARIMA(conditions, order=(5,1,0))  # Ordre (p,d,q) choisi arbitrairement
    fitted_model = model.fit()

    forecast = fitted_model.forecast(steps=6)

    forecast_reversed = forecast[::-1]

    test_model = ARIMA(forecast_reversed, order=(5,1,0))
    test_fitted_model = test_model.fit()
    test_predictions = test_fitted_model.forecast(steps=len(conditions), exog=None)[::-1]

    # Evaluate the model
    r2 = r2_score(conditions, test_predictions)
    mse = mean_squared_error(conditions, test_predictions)
    mae = mean_absolute_error(conditions, test_predictions)

    return (forecast, r2,  mse, mae)

# Créer des séquences d'entraînement et de test
def create_sequences(data, sequence_length):
    X, y = [], []
    for i in range(len(data) - sequence_length):
        X.append(data[i:i+sequence_length])
        y.append(data[i+sequence_length])
    return np.array(X), np.array(y)



def lstm_predictions(conditions):
    print("1---conditions: ", conditions)
    sequence_length = min(2,len(conditions))
    print("sequence_length: ", sequence_length)
    X_train, y_train = create_sequences(conditions, sequence_length)
    print("X_train: ", X_train)
    print("y_train: ", y_train)
    model = Sequential([
        LSTM(units=50, activation='relu', input_shape=(sequence_length, 1)),
        Dense(units=1)
    ])
    model.compile(optimizer='adam', loss='mean_squared_error')
    model.fit(X_train, y_train, epochs=100, batch_size=32)

    # Faire des prédictions pour les deux prochaines années
    last_sequence = np.array(conditions[-sequence_length:])
    predictions = []
    for i in range(6):
        next_prediction = model.predict(last_sequence.reshape(1, sequence_length, 1))
        predictions.append(next_prediction[0][0])
        last_sequence = np.append(last_sequence[1:], next_prediction)

    print("predictions: ", predictions)
    predictions_reversed = predictions[::-1]

    X_test, y_test = create_sequences(predictions_reversed, sequence_length)

    model_test = Sequential([
        LSTM(units=50, activation='relu', input_shape=(sequence_length, 1)),
        Dense(units=1)
    ])
    model_test.compile(optimizer='adam', loss='mean_squared_error')
    model_test.fit(X_test, y_test, epochs=100, batch_size=32)

    # Faire des prédictions pour les deux prochaines années
    last_sequence = np.array(predictions_reversed[-sequence_length:])
    predictions_test = []
    for i in range(len(conditions)):  # dans un an
        next_prediction = model_test.predict(last_sequence.reshape(1, sequence_length, 1))
        predictions_test.append(next_prediction[0][0])
        last_sequence = np.append(last_sequence[1:], next_prediction)

    test_predictions = predictions_test[::-1]

    # Evaluate the model
    print("conditions: ", conditions)
    print("test_ppredictions: ", test_predictions)
    r2 = r2_score(conditions, test_predictions)
    mse = mean_squared_error(conditions, test_predictions)
    mae = mean_absolute_error(conditions, test_predictions)

    return (predictions, r2,  mse, mae)



def best_predictions(r2, mse, mae):
    sum = 0
    for i in [r2,mse,mae]:
        if i >= 0:
            sum+=1
        else:
            sum-=1
    if sum >= 0:
        return 1
    else:
        return 0