from django.urls import path
from . import views

urlpatterns = [
    #path('add/', add_data_view, name='add_data'),
    path('previsions/', views.PredictionApiView.as_view()),
    path('estate-records/', views.EstateRecordsView.as_view(), name='estate-records'),
    path('estates', views.EstatesView.as_view(), name='estates'),
    path('addNew/', views.AddView.as_view(), name='add'),
    path('modify', views.ModifyView.as_view(), name='modify'),
    path('make-record', views.MakeView.as_view()),
    path('modif-record', views.ModifView.as_view())
]