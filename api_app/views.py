# Create your views here.

from django.shortcuts import render, redirect
from django.core.paginator import Paginator, EmptyPage

from rest_framework.views import APIView
from  rest_framework.response import Response

from .prevision import predict_future_prices, get_data, make_record, modif_record

# Create your views here.

class  PredictionApiView(APIView):
    """interface avec le frontend"""

    def get(self, request):
        """renvoyer les prediction d'un immobilier donné"""

        estate_id = request.GET.get('id')
        # Appelez votre fonction avec l'ID de l'Estate
        estates = get_data(estate_id)
        
        # Vérifiez si le resultat a été fourni dans la requête
        if not estates:
            return Response({"error": "Estate ID is not found"}, status=404)
        estates_values = estates.values_list('CRIM', 'ZN', 'INDUS', 'CHAS', 'NOX', 'RM', 'AGE', 'DIS', 'RAD', 'TAX', 'PTRATIO', 'LSTAT', 'MEDV','YEAR')
        
        result, years = predict_future_prices(estates_values)
        # Renvoyez le résultat de la fonction dans la réponse HTTP
        response_data = {
            "result": result,
            "ys": years
        }
        return Response(response_data, status=200)


class  EstateRecordsView(APIView):
    """Tous les estates"""

    def get(self, request):
        """renvoyer les prediction d'un immobilier donné"""

        estate_id = request.GET.get('id')
        # Appelez votre fonction avec l'ID de l'Estate
        estates = get_data(estate_id)
        if  not estates:
            estates = []
        size = request.GET.get('size', 10)
        # Initialiser l'objet Paginator avec tous les biens immobiliers et la taille de la page
        paginator = Paginator(estates, size)

        page = request.GET.get('page', 1)

        try:
            # Récupérer la page spécifiée
            estates_page = paginator.page(page)
            print("estates_page ", estates_page)
        except EmptyPage:
            # Gérer la situation où la page demandée est vide
            estates_page = paginator.page(paginator.num_pages)  # Renvoyer la dernière page
        return render(request, '../templates/estate-records.html', {
            'page_obj': estates_page,
            'id': estate_id,
            'size': size
            })
    
class  EstatesView(APIView):
    """Tous les estates"""

    def get(self, request):
        return render(request, '../templates/estates.html')

class  AddView(APIView):
    """Tous les estates"""

    def get(self, request):

        estate_id = request.GET.get('id')
        # Appelez votre fonction avec l'ID de l'Estate
        estates = get_data(estate_id)
        if  not estates:
            return render(request, '../templates/make-record.html', { 'id': estate_id  })

        stat = request.GET.get('stat')
        if stat == 'New':
            # Calculer l'année appropriée pour le nouvel enregistrement
            latest_year = max(estates, key=lambda x: x.YEAR).YEAR
            year = latest_year + 1
        elif stat == "Prev":
            latest_year = min(estates, key=lambda x: x.YEAR).YEAR
            year = latest_year - 1
        else:
            return render(request, '../templates/404.html')
        # Rediriger vers la page de création d'enregistrement avec l'année calculée
        return render(request, '../templates/make-record-year.html', { 'id': estate_id , 'year': year })

class  ModifyView(APIView):
    """Tous les estates"""

    def get(self, request):

        estate_id = request.GET.get('id')
        year = request.GET.get('year')
        print("pass")
        return render(request, '../templates/make-record-year-modify.html', { 'id': estate_id , 'year': year })


class MakeView(APIView):
    """Endpoint pour ajouter ou mettre à jour une ressource"""

    def post(self, request):
        year = request.data.get('year')
        crim = request.data.get('crim')
        zn = request.data.get('zn')
        id = request.data.get('id')
        indus = request.data.get('indus')
        chas = request.data.get('chas')
        nox = request.data.get('nox')
        rm = request.data.get('rm')
        age = request.data.get('age')
        dis = request.data.get('dis')
        rad = request.data.get('rad')
        tax = request.data.get('tax')
        ptratio = request.data.get('ptratio')
        lstat = request.data.get('lstat')
        medv = request.data.get('medv')
        # Récupérez d'autres champs de formulaire de la même manière
        try:
            make_record(crim,zn,indus,chas,nox,rm,age,dis,rad,tax,ptratio,lstat,medv,year,id)
            return redirect(f'/api/estates')
        except Exception as e:
            return Response({"resp": "error"}, status=500)
        
class ModifView(APIView):
    def post(self, request):
        year = request.data.get('year')
        crim = request.data.get('crim')
        zn = request.data.get('zn')
        id = request.data.get('id')
        indus = request.data.get('indus')
        chas = request.data.get('chas')
        nox = request.data.get('nox')
        rm = request.data.get('rm')
        age = request.data.get('age')
        dis = request.data.get('dis')
        rad = request.data.get('rad')
        tax = request.data.get('tax')
        ptratio = request.data.get('ptratio')
        lstat = request.data.get('lstat')
        medv = request.data.get('medv')
        try:
            modif_record(crim,zn,indus,chas,nox,rm,age,dis,rad,tax,ptratio,lstat,medv,year,id)
            return redirect(f'/api/estate-records?id=${id}&page=1&size=10')
        except Exception as e:
            return Response({"resp": e}, status=500)

"""
from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import VotreModele

def add_data_view(request):
    if request.method == 'POST':
        # Récupérer les données du formulaire
        field1 = request.POST.get('field1')
        field2 = request.POST.get('field2')
        
        # Valider les données (vous devez effectuer une validation appropriée ici)
        
        # Enregistrer les données dans la base de données
        new_data = VotreModele(field1=field1, field2=field2)
        new_data.save()
        
        # Rediriger vers une autre page après l'ajout réussi des données
        return HttpResponseRedirect('/success/')  # Redirigez vers une page de succès ou toute autre page
        
    return render(request, 'your_template.html')
"""