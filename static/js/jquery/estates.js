// Fonction pour rediriger vers la page estate-records.html avec l'ID de l'immobilier sélectionné
function sch(event){
    event.preventDefault();
    loadEstates();
    const searchInput = document.querySelector('.app-search input');
    const searchValue = searchInput.value.trim();
    const estateListing = document.getElementById('estate_listing');
    const estateListItems = estateListing.querySelectorAll('li');
    let f=false;
    estateListItems.forEach(li => {
        const estateIdSpan = li.querySelector('.id-val');
        const estateId = estateIdSpan.textContent.trim(); // Supprimer les espaces blancs inutiles

        // Vérifier si l'ID de l'immobilier correspond à la valeur recherchée
        if (f || estateId !== searchValue) {
            li.style.display = 'none'; // Masquer l'élément s'il ne correspond pas
        } else {
            f=true;
        }
    });
}

function redirectToEstateRecords(estateId) {
    window.location.href = `/api/estate-records?id=${estateId}&page=1&size=10`;
}

function addNewRecord(estateId) {
    window.location.href = `/api/addNew?id=${estateId}&stat=New`;
}

function addPrevRecord(estateId) {
    window.location.href = `/api/addNew?id=${estateId}&stat=Prev`;
}

// Fonction pour charger les estates depuis le backend
function loadEstates() {
    //test
    console.log("Chargement des immeubles");
    data = [{
        estate_id : 1,
    },
    {
        estate_id: 2,
    },
    {
        estate_id: 3,
    }]
    
            const estateListing = document.getElementById('estate_listing');

            data.forEach(estate => {
                const li = document.createElement('li');
                li.innerHTML = `
                    <div class="call-chat">
                        <button class="btn btn-success text-white" type="button" onclick="addNewRecord(${estate.estate_id})">
                            <i class="fas fa-plus text-white"></i>  New Year
                        </button>
                        <button class="btn btn-info text-white" type="button" onclick="addPrevRecord(${estate.estate_id})">
                            <i class="fas fa-plus text-white"></i>  Previous Year
                        </button>
                    </div>
                    <a href="#" onclick="redirectToEstateRecords(${estate.estate_id})" class="d-flex align-items-center">
                        <i class="fas fa-home mb-1 "></i>
                        <div class="ms-2">
                            <span class="text-dark id-val">${estate.estate_id}</span>
                        </div>
                    </a>
                `;
                estateListing.appendChild(li);
            });
    /*Code
    fetch('URL_DU_BACKEND_DISTANT')
        .then(response => response.json())
        .then(data => {
            const estateListing = document.getElementById('estate_listing');

            data.forEach(estate => {
                const li = document.createElement('li');
                li.innerHTML = `
                    <div class="call-chat">
                        <button class="btn btn-success text-white" type="button" onclick="addNewRecord(${estate.estate_id})">
                            <i class="fas fa-plus text-white"></i>  New Year
                        </button>
                        <button class="btn btn-info text-white" type="button" onclick="addPrevRecord(${estate.estate_id})">
                            <i class="fas fa-plus text-white"></i>  Previous Year
                        </button>
                    </div>
                    <a href="{% url 'estate-records' item.id %}" onclick="redirectToEstateRecords(${estate.estate_id})" class="d-flex align-items-center">
                        <i class="fas fa-home mb-1 "></i>
                        <div class="ms-2">
                            <span class="text-dark">${estate.estate_id}</span>
                        </div>
                    </a>
                `;
                estateListing.appendChild(li);
            });
        })
        .catch(error => console.error('Erreur lors de la récupération des données :', error));
    */
}

// Appel de la fonction pour charger les estates au chargement de la page
document.addEventListener('DOMContentLoaded', loadEstates);